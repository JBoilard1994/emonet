Original implementation from : https://github.com/ibab/tensorflow-wavenet

added to the "main" codes :
--train.py
easier to accsss default global variables, used by the arg parser
conditioning modes -> default values in global variables, only WaveNet_mode has to be canged to switch between modes.
eager execution -> tensorflow debug mode

--generate.py
-adapted to mnew trian models

--audio_reader.py
adapted for emotion files
codec2 loader
silence "in the middle" trimmer (in function trim_all_silence(...))
wavenet modes
eager debugging reader (random numbers)
codec2 chunk pooling

model.py
Wavenet modes
local conditioning (did not work, not finished/tested)
DO NOT FORGET to adjust category cardinality for global conditioning (in function get_category_cardinality(files))