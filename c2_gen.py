# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 13:08:44 2018
@author: Jonathan Boilard

Generate codec2 files from source audio
necessites codec2 encode .exe, needs to be made with cmake
"""

from scipy.io.wavfile import write, read
from scipy.signal import resample
import os
import numpy as np

emos = ['Colère', 'Dégoût', 'Joie', 'Peur', 'Surprise', 'Tristesse', 'Neutre']
strengths = ['Faible', 'Fort']

os.mkdir('CaFE_c2')
for emo in emos:
    os.mkdir(os.path.join('CaFE_c2', emo))
    if (not emo == 'Neutre'):   
        for strength in strengths:
            os.mkdir(os.path.join('CaFE_c2', emo, strength))

for emo in emos:
    if (not emo == 'Neutre'):   
        for strength in strengths:
        
            path = os.path.join(os.getcwd(), 'CaFE_8k_emotions', emo, strength)
            files = os.listdir(path)
            
            for filename in files:
                command =   'codec2\c2enc.exe 3200 ' + os.path.join(os.getcwd(), 'CaFE_8k_emotions', emo, strength, filename) + ' ' + os.path.join(os.getcwd(), 'CaFE_c2', emo, strength, filename.split('.')[0] + '.c2')       
                os.system(command)
    else:
        path = os.path.join(os.getcwd(), 'CaFE_8k_emotions', emo)
        files = os.listdir(path)
            
        for filename in files:
            command =   'codec2\c2enc.exe 3200 ' + os.path.join(os.getcwd(), 'CaFE_8k_emotions', emo, filename) + ' ' + os.path.join(os.getcwd(), 'CaFE_c2', emo, filename.split('.')[0] + '.c2')       
            os.system(command)
    




