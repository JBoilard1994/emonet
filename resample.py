# -*- coding: utf-8 -*-
"""
@author: boij3114
Created on Wed Jun 13 13:08:44 2018
Resample source 48k audio into a lower sampling target, in this case, it's 8k.
"""

from scipy.io.wavfile import write, read
from scipy.signal import resample
import os
import numpy as np

SOURCE_SAMPLING = 48000
TARGET_SAMPLING = 8000

emos = ['Colère', 'Dégoût', 'Joie', 'Peur', 'Surprise', 'Tristesse', 'Neutre']
strengths = ['Faible', 'Fort']

os.mkdir('CaFE_8k_emotions')
for emo in emos:
    os.mkdir(os.path.join('CaFE_8k_emotions', emo))
    
    if (not emo == 'Neutre'):  
        for strength in strengths:
            os.mkdir(os.path.join('CaFE_8k_emotions', emo, strength))

for emo in emos:
    
    if (not emo == 'Neutre'):   # Neutral emotion has no Strength
        for strength in strengths:
        
            path = os.path.join(os.getcwd(), 'CaFE_48k_emotions', emo, strength)
            files = os.listdir(path)
            
            for filename in files:
                fs,y = read(os.path.join(path,filename))
                y2 = resample(y, int(len(y)/(SOURCE_SAMPLING/TARGET_SAMPLING)))
                
                write(os.path.join('CaFE_8k_emotions', emo, strength, filename), TARGET_SAMPLING, y2.astype(np.int16))
                print('saved ' + filename)
    else:
        path = os.path.join(os.getcwd(), 'CaFE_48k_emotions', emo)
        files = os.listdir(path)
            
        for filename in files:
            fs,y = read(os.path.join(path,filename))
            y2 = resample(y, int(len(y)/6))
            
            write(os.path.join('CaFE_8k_emotions', emo, filename), TARGET_SAMPLING, y2.astype(np.int16))
            print('saved ' + filename)    
    




