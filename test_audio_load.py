# -*- coding: utf-8 -*-
"""
Created on Tue May 29 11:54:04 2018

@author: boij3114
"""

# -*- coding: utf-8 -*-
"""
Created on Fri May 25 14:41:43 2018

@author: boij3114
"""
import fnmatch
import os
import random
import re
import threading

import librosa
import numpy as np
import tensorflow as tf

import sys
sys.path.insert(0, './codec2')
import codec2

import matplotlib.pyplot as plt


FILE_PATTERN = r'(\d{2})-([A-Z])-(\d)-(\d).wav'
emo_names = ['Colère', 'Dégoût', 'Joie', 'Peur', 'Surprise', 'Tristesse']
emo = ['C', 'D', 'J', 'P', 'S', 'T']
strength = ['Faible', 'Fort']

sample_rate = 48000
treshold = 0.01
save_path = os.path.join(os.getcwd(), 'cafe_test')

def find_files(directory, pattern='*.wav'):
    '''Recursively finds all files matching the pattern.'''
    files = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, pattern):
            files.append(os.path.join(root, filename))
    return files

def trim_all_silence(audio_temp, threshold, frame_length=2048, max_silence_length=16000, begin_trim_margin = 6000, end_trim_margin = 6000, init_trim_margin = 16000, final_trim_margin = 16000):
    '''Removes silence at the beginning and end of a sample.'''

    if audio.size < frame_length:
        frame_length = audio.size
   
    energy = librosa.feature.rmse(audio, frame_length=frame_length)
    frames = np.nonzero(energy > threshold)
    indices = librosa.core.frames_to_samples(frames)[1]
    
    print(indices)
    
    last_ind = indices[0]
    first_ind = last_ind - init_trim_margin + begin_trim_margin
    total_audio = []
   
    for i in range(1,indices.size):
        new_ind = indices[i]
        if new_ind - last_ind > max_silence_length:
            total_audio = np.concatenate((total_audio, audio[(first_ind - begin_trim_margin): (last_ind + end_trim_margin)]), axis=0)
            first_ind = new_ind
        last_ind = new_ind
       
    total_audio = np.concatenate((total_audio, audio[(first_ind - begin_trim_margin) : (last_ind + final_trim_margin)]), axis=0)
        
    # Note: indices can be an empty array, if the whole audio was silence.
    return total_audio


def load_generic_audio(directory, sample_rate):
    '''Generator that yields audio waveforms from the directory.'''
    files = find_files(directory)
    id_reg_exp = re.compile(FILE_PATTERN)

    for filename in files:
        ids = id_reg_exp.findall(filename)
        if not ids:
        # The file name does not match the pattern containing ids, so
        # there is no id.
            category_id = None
        else:
            # The file name matches the pattern for containing ids.
            category_id = 0
            
        audio, _ = librosa.load(filename, sr=sample_rate, mono=True)
        yield audio, filename, category_id

def load_c2_file(filepath):
    id_reg_exp = re.compile(FILE_PATTERN)
    filename = filepath.split('\\')[-1]
    ids = id_reg_exp.findall(filename)
    
    emo_id = emo.index(ids[0][1])
    strength_id = int(ids[0][2]) - 1
    
    emo_name = emo_names[emo_id]
    str_name = strength[strength_id]
    
    filename = filename.split('.')[0]
    if emo_name == 'Neutre':
        c2_path = os.path.join(os.getcwd(), emo_name, filename + '.c2')
    else:
        c2_path = os.path.join(os.getcwd(), 'CaFE_c2', emo_name, str_name, filename + '.c2')
    
    models = codec2.read_c2_file(c2_path)
    return models

def codec2_audio_pooling(self, audio, models):
        n_window = len(models)
        i = random.randint(0, n_window-1)
        model = models[i]
        data = model.normalize()
    
        nsamples = self.sample_rate/100
        piece = audio[i*nsamples, i*(nsamples+1) + self.receptive_field]
        
        return [piece, data]

directory = os.path.join(os.getcwd(), "CaFE_16k_emotions", "Tristesse", "Fort")
iterator = load_generic_audio(directory, 16000)
id_reg_exp = re.compile(FILE_PATTERN)

i=0
for audio, filepath, category_id in iterator:
    
    plt.figure()
    plt.plot(audio)
    
    models = load_c2_file(filepath)
    ids = id_reg_exp.findall(filepath)
    if ids:
        # The file name matches the pattern for containing ids.
         
       
        audio = audio.reshape(-1, 1)
        if audio.size == 0:
            print("Warning: {} was ignored as it contains only "
                  "silence. Consider decreasing trim_silence "
                  "threshold, or adjust volume of the audio."
                  .format(filepath))
    
    audio = np.pad(audio, [[1024, 0], [0, 0]],
                   'constant')
    

            
    
       
        

    
    
    
