# -*- coding: utf-8 -*-
from enum import Enum
class WavenetModes(Enum):
    NO_CONDITIONING             = 0
    EMO_GLOBAL_CONDTIONING      = 1
    CODEC2_LOCAL_CONDITIONING   = 2
