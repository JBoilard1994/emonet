import numpy as np
from codebook import get_codes

MAX_AMP = 80    # MAX number of harmonics
N = 80          # number of samples per frame
M = 320         # pitch analysis frame size
LPC_ORD = 10    # LPC order

LSPD_SCALAR_INDEXES = 10

LSP_BITS = 5
LSP_K = 1           # dimension of LSP VECTOR
LSP_M = 32          # ELEMENTS in LSP codebook

E_BITS = 5
E_MAX_DB = 40.0
E_MIN_DB = -10.0
E_LEVELS = 2**E_BITS

WO_BITS = 7
P_MAX = 160     # MAX pitch
P_MIN = 20      # Min Pitch

## configured for codec2-3200
NSAM = 160      #Speech Samples per Frame
NBIT = 64       #nbits per frame


class C2AudioModel(object):
     # initialize at previous model values
    def __init__(self, Wo = (2*np.pi)/P_MAX, L = (np.pi*P_MAX)/(2*np.pi), phi=np.zeros(MAX_AMP + 1), voiced = 0, e=1):
         self.Wo = Wo
         self.L = L
         self.phi = phi
         self.voiced = voiced
         self.e = e
         
         lsp = (np.ones(LPC_ORD)*np.pi)/(LPC_ORD + 1)
         for i in range(0,LPC_ORD):
             lsp[i] = lsp[i]*i
             
         self.lsp = lsp
         self.dlsp = lsp
 
    #Normalize all model parameters between -1 and 1
    def normalize(self):     
        data = np.zeros(14)
        codes = get_codes()
        
        data[0] = ((self.Wo - (2*np.pi)/P_MAX)/((2*np.pi)/P_MIN - (2*np.pi)/P_MAX))*2 - 1
        data[1] = ((self.L - (np.pi*P_MIN)/(2*np.pi))/((np.pi*P_MAX)/(2*np.pi) - (np.pi*P_MIN)/(2*np.pi)))*2 - 1
        data[2] = self.voiced*2 -1
        data[3] = ((self.e - 10**(E_MIN_DB/10))/(10**(E_MAX_DB/10)-10**(E_MIN_DB/10)))*2 - 1
        
        for i in range(0,LPC_ORD):
            data[4+i] = ((self.dlsp[i] - codes[i][0])/(codes[i][-1] - codes[i][0]))*2 - 1 
            
        return np.reshape(data, (1,14))            
    
def lsp_min_max_values(order):
    lsp_min = np.zeros(10)
    lsp_max = np.zeros(10)
    
    codes = get_codes()
    
    for i in range(0,order):
        cb = codes[i]
        value_min = cb[0]
        value_max = cb[-1]
        
        if i:
            lsp_min[i] = lsp_min[i-1] + value_min
            lsp_max[i] = lsp_max[i-1] + value_max
        else:
            lsp_min[0] = value_min
            lsp_max[0] = value_max
        
    lsp_min = (np.pi/4000.0)*lsp_min
    lsp_max = (np.pi/4000.0)*lsp_max
    
    return [lsp_min, lsp_max]
 
def decode_lspds_scalar(indexes, order):
    codes = get_codes()
    
    dlsp_ = np.zeros(order)
    lsp__hz = np.zeros(order)
    lsp_ = np.zeros(order)
    
    for i in range(0,order):
        cb = codes[i]
        dlsp_[i] = cb[int(indexes[i]*LSP_K)]
        
        if i:
            lsp__hz[i] = lsp__hz[i-1] + dlsp_[i]
        else:
            lsp__hz[0] = dlsp_[0]
            
    lsp_ = (np.pi/4000.0)*lsp__hz
            
    return [lsp_, dlsp_]

def decode_Wo(index, nbits):
    Wo_min = (2*np.pi)/P_MAX
    Wo_max = (2*np.pi)/P_MIN
    Wo_levels = 2**nbits
    
    step = (Wo_max - Wo_min)/Wo_levels
    Wo = Wo_min + step*index
    
    return Wo
    
def decode_energy(index, nbits):
    
    step = (E_MAX_DB - E_MIN_DB)/E_LEVELS
    e_Db = E_MIN_DB + step*index
    e = 10**(e_Db/10)
    
    return e
    
def unpack(bits, ibit, nbit):
    i = 0
    value = 0
    
    bin_value = bits[ibit: ibit+nbit]
    for bit in reversed(bin_value):
        value = value + bit*(2**i)
        i = i+1
        
    return [value, ibit + nbit]

def interp_Wo(interp, prev_, next_, weight=0.5):
    
    #trap edge case where voicing estimation is most probably wrong
    if(interp.voiced and not prev_.voiced and not next_.voiced):
        interp.voiced = 0
        
    if(interp.voiced):
        if (prev_.voiced and next_.voiced):
            interp.Wo = (1.0-weight)*prev_.Wo + weight*next_.Wo
        if (not prev_.voiced and next_.voiced):
            interp.Wo = next_.Wo
        if (prev_.voiced and not next_.voiced):
            interp.Wo = prev_.Wo
    else:
        interp.Wo = (np.pi*2)/P_MAX
    
    interp.L = np.pi/interp.Wo
     
    return interp    

def interp_energy(prev_e, next_e):
    return 10**((np.log10(prev_e) + np.log10(next_e))/2)

def interp_lsp(prev_lsp, next_lsp, prev_dlsp, next_dlsp, weight = 0.5, order = LPC_ORD):
    lsp = (1.0-weight)*prev_lsp + weight*next_lsp
    dlsp = (1.0-weight)*prev_dlsp + weight*next_dlsp

    return [lsp, dlsp]

    
def read_c2_file(filename):
    nbyte = int((NBIT + 7)/8)
    lspd_indexes = np.zeros(LPC_ORD)
    f = open(filename)
    all_bytes = np.fromfile(f, dtype=np.uint8)
    
    models = []
    previous_model = C2AudioModel()
    
    # for each window...
    for i in range (0, int(len(all_bytes)/nbyte)):
        ibit = 0
        models.append(C2AudioModel())
        models.append(C2AudioModel())
        
        bytes_ = all_bytes[i*nbyte : (i+1)*nbyte]
        
        bits = np.unpackbits(bytes_)
        
        [models[2*i].voiced, ibit] = unpack(bits, ibit, 1)
        [models[2*i + 1].voiced, ibit] = unpack(bits, ibit, 1)
        
        [Wo_index, ibit] = unpack(bits, ibit, WO_BITS)
        models[2*i + 1].Wo = decode_Wo(Wo_index, WO_BITS)
        models[2*i + 1].L = np.pi/models[2*i + 1].Wo
        
        [e_index, ibit] = unpack(bits, ibit, E_BITS)
        models[2*i + 1].e = decode_energy(e_index, E_BITS)
        
        for n in range(0,LSPD_SCALAR_INDEXES):
            [lspd_indexes[n], ibit] = unpack(bits, ibit, 5)
         
        [models[2*i + 1].lsp, models[2*i + 1].dlsp] = decode_lspds_scalar(lspd_indexes, LPC_ORD)
        
        #fill model0 with interpolation
        models[2*i] = interp_Wo(models[2*i], previous_model, models[2*i + 1])
        models[2*i].e = interp_energy(previous_model.e, models[2*i + 1].e)
        
        [models[2*i].lsp, models[2*i].dlsp] = interp_lsp(previous_model.lsp, models[2*i + 1].lsp, previous_model.dlsp, models[2*i + 1].dlsp)
        
        previous_model = models[2*i + 1]
        
    return models


         