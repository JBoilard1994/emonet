# -*- coding: utf-8 -*-
def get_codes():
    _codes0 = [25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800]
    _codes1 = _codes0
    _codes2 = _codes0
    _codes3 = [25,50,75,100,125,150,175,200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400]
    _codes4 = _codes3
    _codes5 = _codes3
    _codes6 = _codes0
    _codes7 = _codes0
    _codes8 = _codes0
    _codes9 = _codes0
    
    codes = [_codes0, _codes1, _codes2, _codes3, _codes4, _codes5, _codes6, _codes7, _codes8, _codes9]
    
    return codes
