import fnmatch
import os
import random
import re
import threading

import librosa
import numpy as np
import tensorflow as tf
import random


import sys
sys.path.insert(0, './codec2')
sys.path.insert(0, './../')
from codec2 import read_c2_file, C2AudioModel
from defines import WavenetModes

FILE_PATTERN = r'(\d{2})-([A-Z])-(\d)-(\d).wav'
emo_names = ['Colère', 'Dégoût', 'Joie', 'Peur', 'Surprise', 'Tristesse', 'Neutre']
emo_list = ['C', 'D', 'J', 'P', 'S', 'T', 'N']
strength = ['Faible', 'Fort']

# HARD-CODED
def get_category_cardinality(files):

    strength_id_list = [0, 1]
    sex_id_list = [0, 1]  
    
    max_id = 13
    
# =============================================================================
#     for filename in files:
#         matches = id_reg_expression.findall(filename)
#         id, recording_id = [int(id_) for id_ in matches]
#         if min_id is None or id < min_id:
#             min_id = id
#         if max_id is None or id > max_id:
#             max_id = id
# =============================================================================

    return max_id

# return random samples

    

def randomize_files(files):
    for file in files:
        file_index = random.randint(0, (len(files) - 1))
        yield files[file_index]


def find_files(directory, pattern='*.wav'):
    '''Recursively finds all files matching the pattern.'''
    files = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, pattern):
            files.append(os.path.join(root, filename))
    return files


def load_generic_audio(directory, sample_rate):
    '''Generator that yields audio waveforms from the directory.'''
    files = find_files(directory)
    id_reg_exp = re.compile(FILE_PATTERN)
    print("files length: {}".format(len(files)))
    randomized_files = randomize_files(files)
    for filename in randomized_files:
        ids = id_reg_exp.findall(filename)
        if not ids:
        # The file name does not match the pattern containing ids, so
        # there is no id.
            category_id = None
        else:
            # The file name matches the pattern for containing ids.
            
            sex_id = int(ids[0][0])%2 #male = 1, female = 0
            emo_id = emo_list.index(ids[0][1]) 
           # strength_id = int(ids[0][2]) - 1
            
            category_id = emo_id*2 + sex_id
            
        audio, _ = librosa.load(filename, sr=sample_rate, mono=True)
        audio = audio.reshape(-1, 1)
        yield audio, filename, category_id
        
def load_c2_file(filepath):
    id_reg_exp = re.compile(FILE_PATTERN)
    filename = filepath.split('\\')[-1]
    ids = id_reg_exp.findall(filename)
    
    emo_id = emo_list.index(ids[0][1])
    strength_id = int(ids[0][2]) - 1
    
    emo_name = emo_names[emo_id]
    str_name = strength[strength_id]
    
    print(filename)
    
    filename = filename.split('.')[0]
    if emo_name == 'Neutre':
        c2_path = os.path.join(os.getcwd(), 'CaFE_c2', emo_name, filename + '.c2')
    else:
        c2_path = os.path.join(os.getcwd(), 'CaFE_c2', emo_name, str_name, filename + '.c2')
    
    models = read_c2_file(c2_path)
    return models

'''Removes silence at the beginning and end of a sample.'''
def trim_silence(audio, threshold, frame_length=2048):   
    if audio.size < frame_length:
        frame_length = audio.size
    energy = librosa.feature.rmse(audio, frame_length=frame_length)
    frames = np.nonzero(energy > threshold)
    indices = librosa.core.frames_to_samples(frames)[1]

    # Note: indices can be an empty array, if the whole audio was silence.
    return audio[indices[0]:indices[-1]] if indices.size else audio[0:0]

'''Removes silence at the beginning and end and middles of a sample.'''
def trim_all_silence(audio, threshold, frame_length=2048, max_silence_length=16000, begin_trim_margin = 6000, end_trim_margin = 6000, init_trim_margin = 2000, final_trim_margin = 2000):
    if audio.size < frame_length:
        frame_length = audio.size
   
    energy = librosa.feature.rmse(audio, frame_length=frame_length)
    frames = np.nonzero(energy > threshold)
    indices = librosa.core.frames_to_samples(frames)[1]
    
    last_ind = indices[0]
    first_ind = last_ind - init_trim_margin + begin_trim_margin
    total_audio = []
   
    for i in range(1,indices.size):
        new_ind = indices[i]
        if new_ind - last_ind > max_silence_length:
            total_audio = np.concatenate((total_audio, audio[(first_ind - begin_trim_margin): (last_ind + end_trim_margin)]), axis=0)
            first_ind = new_ind
        last_ind = new_ind
       
    total_audio = np.concatenate((total_audio, audio[(first_ind - begin_trim_margin) : (last_ind + final_trim_margin)]), axis=0)
        
    # Note: indices can be an empty array, if the whole audio was silence.
    return total_audio


def not_all_have_id(files):
    ''' Return true iff any of the filenames does not conform to the pattern
        we require for determining the category id.'''
    id_reg_exp = re.compile(FILE_PATTERN)
    for file in files:
        ids = id_reg_exp.findall(file)
        if not ids:
            return True
    return False


class AudioReader(object):
    '''Generic background audio reader that preprocesses audio files
    and enqueues them into a TensorFlow queue.'''

    def __init__(self,
                 audio_dir,
                 codec2_dir,
                 coord,
                 sample_rate,
                 wavenet_mode,
                 receptive_field,
                 sample_size=None,
                 silence_threshold=None,
                 queue_size=32,
                 eager_debugging = False,
                 gc_cardinality = None):
        self.codec2_dir = codec2_dir
        self.audio_dir = audio_dir
        self.sample_rate = sample_rate
        self.coord = coord
        self.sample_size = sample_size
        self.receptive_field = receptive_field
        self.silence_threshold = silence_threshold
        self.wavenet_mode = wavenet_mode
        self.eager_debugging = eager_debugging
        self.threads = []
        
        if not eager_debugging:
            self.sample_placeholder = tf.placeholder(dtype=tf.float32, shape=None)
            self.queue = tf.PaddingFIFOQueue(queue_size,
                                             ['float32'],
                                             shapes=[(None, 1)])
            self.enqueue = self.queue.enqueue([self.sample_placeholder])
    
            if self.wavenet_mode == WavenetModes.EMO_GLOBAL_CONDTIONING:
                self.id_placeholder = tf.placeholder(dtype=tf.int32, shape=()) #Inserts a placeholder for a tensor that will be always fed.
                self.gc_queue = tf.PaddingFIFOQueue(queue_size, ['int32'],
                                                    shapes=[()])
                self.gc_enqueue = self.gc_queue.enqueue([self.id_placeholder]) #add to queue this item
                
            if self.wavenet_mode == WavenetModes.CODEC2_LOCAL_CONDITIONING:
                self.codec2_placeholder = tf.placeholder(dtype=tf.float32, shape = (1,14))
                self.lc_queue = tf.PaddingFIFOQueue(queue_size, ['float32'],shapes=[(1,14)])
                self.lc_enqueue = self.lc_queue.enqueue([self.codec2_placeholder]) #add to queue this item
            

        # TODO Find a better way to check this.
        # Checking inside the AudioReader's thread makes it hard to terminate
        # the execution of the script, so we do it in the constructor for now.
        files = find_files(audio_dir)
        if not files:
            raise ValueError("No audio files found in '{}'.".format(audio_dir))
        if self.wavenet_mode == WavenetModes.EMO_GLOBAL_CONDTIONING and not_all_have_id(files):
            raise ValueError("Global conditioning is enabled, but file names "
                             "do not conform to pattern having id.")
        # Determine the number of mutually-exclusive categories we will
        # accomodate in our embedding table.
        if self.wavenet_mode == WavenetModes.EMO_GLOBAL_CONDTIONING:
            if gc_cardinality == None:
                self.gc_category_cardinality = get_category_cardinality(files) #max speaker ID
                # Add one to the largest index to get the number of categories,
                # since tf.nn.embedding_lookup expects zero-indexing. This
                # means one or more at the bottom correspond to unused entries
                # in the embedding lookup table. But that's a small waste of memory
                # to keep the code simpler, and preserves correspondance between
                # the id one specifies when generating, and the ids in the
                # file names.
                self.gc_category_cardinality += 1
                print("Detected --gc_cardinality={}".format(
                      self.gc_category_cardinality))
            else:
                self.gc_category_cardinality = gc_cardinality  
        else:
            self.gc_category_cardinality = None
     
    #return garbage data to debug graph with EAGER model generation
    def eager_reader(self):  
        gc_id = None
        lc_codec2 = None
                
        samples = tf.random_uniform([self.receptive_field + self.sample_size],  minval = -1, maxval = 1)
        if self.wavenet_mode == WavenetModes.EMO_GLOBAL_CONDTIONING:
            gc_id = tf.random_uniform([1],  minval = 0, maxval = self.gc_category_cardinality, dtype=tf.int32)
        if self.wavenet_mode == WavenetModes.CODEC2_LOCAL_CONDITIONING:
            lc_codec2 = tf.random_uniform([14],  minval = -1, maxval = 1, dtype=tf.float32)
            
        return [samples, gc_id, lc_codec2]
            
    def dequeue(self, num_elements):
        output = self.queue.dequeue_many(num_elements)
        return output

    def dequeue_gc(self, num_elements):
        return self.gc_queue.dequeue_many(num_elements)
    
    def dequeue_lc(self, num_elements):
        return self.lc_queue.dequeue_many(num_elements)

    #returns audio file of length self.sample_size or less, which would concur with the whole file.
    def audio_pooling(self, audio):
        return audio[:(self.receptive_field + self.sample_size), :]

    #returns audio sample of a single codec2 frame length
    def codec2_audio_pooling(self, audio, models):
        n_window = len(models)
        i = random.randint(0, n_window-2)
        model = models[i]
        data = model.normalize()
        
        nsamples = int(self.sample_rate/100)
        piece = audio[i*nsamples: (i+1)*(nsamples) + self.receptive_field]
        
        return [piece, data]

    
    def thread_main(self, sess):
        stop = False
        # Go through the dataset multiple times
        while not stop:
            iterator = load_generic_audio(self.audio_dir, self.sample_rate)
            
            
            for audio, filepath, category_id in iterator:
                
                if self.coord.should_stop():
                    stop = True
                    break
                if self.silence_threshold is not None:
                    # Remove silence  
                    #no trim silence for local codec2
                    if self.wavenet_mode == WavenetModes.NO_CONDITIONING or self.wavenet_mode == WavenetModes.EMO_GLOBAL_CONDTIONING:
                        audio = trim_silence(audio[:, 0], self.silence_threshold)
                        audio = audio.reshape(-1, 1)
  
                    if audio.size == 0:
                        print("Warning: {} was ignored as it contains only "
                              "silence. Consider decreasing trim_silence "
                              "threshold, or adjust volume of the audio."
                              .format(filepath))

                audio = np.pad(audio, [[self.receptive_field, 0], [0, 0]],
                               'constant')
                # Cut samples into pieces of size receptive_field +
                # sample_size with receptive_field overlap
                while len(audio) > self.receptive_field:
                    
                    if self.wavenet_mode == WavenetModes.NO_CONDITIONING or self.wavenet_mode == WavenetModes.EMO_GLOBAL_CONDTIONING:
                        
                        piece = self.audio_pooling(audio)
                        audio = audio[self.sample_size:, :]
                    elif self.wavenet_mode == WavenetModes.CODEC2_LOCAL_CONDITIONING:
                        #force stochastic gradient by presenting a single codec2 window frame batch from the current file
                        models = load_c2_file(filepath)
                        [piece, codec2_data] = self.codec2_audio_pooling(audio, models)
                        audio = [0]
                        
                        
                    sess.run(self.enqueue,
                             feed_dict={self.sample_placeholder: piece})
                    
                    
                    if self.wavenet_mode == WavenetModes.EMO_GLOBAL_CONDTIONING:
                        sess.run(self.gc_enqueue, feed_dict={
                            self.id_placeholder: category_id})
    
                    if self.wavenet_mode == WavenetModes.CODEC2_LOCAL_CONDITIONING:
                        sess.run(self.lc_enqueue, feed_dict={
                            self.codec2_placeholder: codec2_data})


    def start_threads(self, sess, n_threads=1):
        for _ in range(n_threads):
            thread = threading.Thread(target=self.thread_main, args=(sess,))
            thread.daemon = True  # Thread will close when parent quits.
            thread.start()
            self.threads.append(thread)
        return self.threads
